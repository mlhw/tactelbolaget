# README #

## Tactel Arbetsprov ##
Systembolaget iOS app   
v 1.0 

### Details ###

* iOS app written in Swift
* Deployment Target: 10.2

### Installation instructions ###

* Open project in Xcode
* Run in Simulator or device

* Filter ecological procuts by using "**iseco**" search string
* Use Swedish flag emoji to search for Swedish articles
* Shake device for random selection