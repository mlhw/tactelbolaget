//
//  MasterViewController.swift
//  Systembolaget
//
//  Created by Mattias Lundhaw on 2017-03-08.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UISearchResultsUpdating {

    var detailViewController: DetailViewController? = nil
    var objects = [Article]()
    var filteredObjects = [Article]()
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let article:Article?
            if let item = sender as? Article {
                article = item
            } else if let indexPath = self.tableView.indexPathForSelectedRow {
                let articles:[Article]
                if searchController.isActive && searchController.searchBar.text != "" {
                    articles = filteredObjects
                } else {
                    articles = objects
                }
                
                article = articles[indexPath.row] //as! Article
            } else {
                article = nil
            }
            
            if let object = article {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredObjects.count
        }
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object:Article
        if searchController.isActive && searchController.searchBar.text != "" {
            object = filteredObjects[indexPath.row]
        } else {
            object = objects[indexPath.row]
        }
        
        cell.textLabel!.text = object.Namn
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    // MARK: fetch data
    
    func fetchData() {
        self.showActivityIndicator()
        
        let url = URL(string: "https://www.systembolaget.se/api/assortment/products/xml")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {
                // TODO: handle error
                return
            }
            
            guard let xmlStr = String(data: data, encoding: .utf8) else {
                // TODO: handle error
                return
            }
            
            self.parse(xmlString: xmlStr)
            
        }.resume()
    }
    
    func parse(xmlString:String) {
        let parser = ArticleParser(xml: xmlString)
        let articles = parser.parseXML() // array of articles
        self.objects = articles
        
        DispatchQueue.main.async(execute: {
            // Remove activity indicator
            self.navigationItem.rightBarButtonItem = nil
            // Update view
            self.tableView.reloadData()
        })

    }

    /// Add an activityIndicator to the navigation bar
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let barButtonItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.setRightBarButton(barButtonItem, animated: true)
        activityIndicator.startAnimating()
    }
    
    // MARK: UISearchController methods
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(for: searchController.searchBar.text!)
    }
    
    /// Filter the content by the search string in the search bar 
    func filterContent(for searchText: String, scope: String = "All") {
        switch searchText.lowercased() {
        case "🇸🇪":
            filteredObjects = objects.filter { article in
                return article.Ursprunglandnamn.lowercased() == "sverige"
            }
        case "iseko":
            filteredObjects = objects.filter { article in
                return article.Ekologisk == "1"
            }
        default:
            filteredObjects = objects.filter { article in
                return article.Namn.lowercased().contains(searchText.lowercased())
            }
        }
        
        tableView.reloadData()
    }
    
    // MARK: Extras
    
    /// Shake to select random item
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        print("Device was shaken!")
        
        // Make sure there is more than one item
        guard objects.count > 1 else {
            return
        }
        
        // Get a random item
        let randomNum = Int(arc4random_uniform(UInt32(objects.count)))
        let item = objects[randomNum];
        
        // Show the item details
        performSegue(withIdentifier: "showDetail", sender: item)
        
    }
}



