//
//  DetailViewController.swift
//  Systembolaget
//
//  Created by Mattias Lundhaw on 2017-03-08.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var articleIdLabel: UILabel!
    @IBOutlet weak var prisLabel: UILabel!
    @IBOutlet var ekoLabel: UILabel!
    @IBOutlet weak var coutnryLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        guard let detail = self.detailItem, let _ = self.articleIdLabel else {
            // Make sure the detail item exists 
            // Make sure that the labels exist. 
            // Safer would be to check that all labels exists to save time we asume :-)
            
            self.title = ""
            self.articleIdLabel?.text = ""
            self.prisLabel?.text = ""
            self.ekoLabel?.text = ""
            self.coutnryLabel?.text = ""
            return
        }
        self.title = detail.Namn
        self.articleIdLabel.text = detail.Artikelid
        self.prisLabel.text = detail.Prisinklmoms
        self.ekoLabel.text = detail.Ekologisk=="1" ? "Ja":"Nej"
        self.coutnryLabel.text = detail.Ursprunglandnamn
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Article? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }


}

