//
//  Article.swift
//  Systembolaget
//
//  Created by Mattias Lundhaw on 2017-03-08.
//  Copyright © 2017 Lundhaw. All rights reserved.
//

import UIKit

/// This class represents an article from Systembolaget
class Article: NSObject {
    var nr: String = String()
    var Artikelid: String = ""
    var Varnummer: String = ""
    var Namn: String = ""
//    var Namn2: String
    var Prisinklmoms: String = ""
//    var Volymiml: String
//    var PrisPerLiter: String
//    var Saljstart: String
//    var Utgått: Int
//    var Varugrupp: String
//    var Typ: String
//    var Stil: String
//    var Forpackning: String
//    var Forslutning: String
//    var Ursprung: String
    var Ursprunglandnamn: String = ""
//    var Producent: String
//    var Leverantor: String
//    var Argang: String
//    var Provadargang: String
//    var Alkoholhalt: String
//    var Sortiment: String
//    var SortimentText: String
    var Ekologisk: String = ""
//    var Etiskt: String
    var Koscher: String = ""
//    var RavarorBeskrivning: String
    


}

/// The parser that parses the XML into articles
class ArticleParser: NSObject {
    var parser: XMLParser
    
    /// List of articles
    var articles = [Article]()
    /// current article being parsed
    var article:Article?
    /// The current element name
    var currentElement:String = ""
    
    /// Init the parser
    /// - parameters:
    ///     - xml: The xml string to parse
    init(xml: String) {
        parser = XMLParser(data: xml.data(using: String.Encoding.utf8)!)
        super.init()
        parser.delegate = self
    }
    
    /// Start parsing the XML
    func parseXML() -> [Article] {
        parser.parse()
        return articles
    }
    
}

extension ArticleParser: XMLParserDelegate {
    
    // Start Element
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        self.currentElement = elementName
        
        if elementName == "artikel" {
            self.article = Article()
        }
    }
    
    
    // End Element
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "artikel" {
            if let article = self.article {
                articles.append(article)
                print("Parsed: \(self.articles.count)")
            }
        }
    }
    
    // parse characters
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if (!data.isEmpty) {
            switch currentElement {
            case "nr":
                self.article?.nr += data
            case "Namn":
                self.article?.Namn += data
            case "Artikelid":
                self.article?.Artikelid += data
            case "Ekologisk":
                self.article?.Ekologisk += data
            case "Koscher":
                self.article?.Koscher += data
            case "Prisinklmoms":
                self.article?.Prisinklmoms += data
            case "Ursprunglandnamn":
                self.article?.Ursprunglandnamn += data
            default: break
                // Ignore value
            }
        }
    }
}
